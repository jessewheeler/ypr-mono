trigger UpdateChapterScoreOnCall on Chapter_Call_Check_in__c (before insert) {
    for (Chapter_Call_Check_in__c call : Trigger.new) {
        
        // Setup variables for trigger
        String chapterId   = call.Account__c;
        DateTime created   = System.now();
        Date dateOfCall    = date.newInstance(created.year(),created.month(),created.day());
        Decimal score      = call.Score__c;
        Decimal callScore  = 0.5;
        
         // Get Chapter
        Account chapter = [SELECT Id,Quarterly_Call_Score__c,Start_Of_Quarter__c,End_Of_Quarter__c FROM Account WHERE Id = :chapterId];
        
        // Pull chapter values
        Decimal chapterCallScore = chapter.Quarterly_Call_Score__c;
        Date startOfQuarter      = chapter.Start_Of_Quarter__c;
        Date endOfQuarter        = chapter.End_Of_Quarter__c;

        if (dateOfCall > startOfQuarter && dateOfCall < endOfQuarter && dateOfCall != null) {
            
            // Update chapter score to 0 if null
            if (chapterCallScore == null) {
                chapterCallScore = 0;
            }

            // If call falls within current quarter, update Chapter's call score
            // System.debug(chapterCallScore);
            // System.debug(callScore);
            chapterCallScore                = chapterCallScore + callScore;
            chapter.Quarterly_Call_Score__c = chapterCallScore;
            call.Score__c                   = callScore;
            update chapter;
        }
    }
}