trigger UpdateChapterScore on Chapter_Event__c (before insert, before update) {
    for (Chapter_Event__c event : Trigger.new) {
        
        // Setup variables for trigger
        String chapterId     = event.Chapter__c;
        Date dateOfEvent     = event.Event_Date__c;
        String eventType     = event.Event_Type__c;
        Decimal eventScore   = event.Score__c;
        
         // Get Chapter
        Account chapter = [SELECT Id,Score__c,Start_Of_Quarter__c,End_Of_Quarter__c FROM Account WHERE Id = :chapterId];
        Decimal chapterScore = chapter.Score__c;
        Date startOfQuarter  = chapter.Start_Of_Quarter__c;
        Date endOfQuarter    = chapter.End_Of_Quarter__c;

        // Determine if event falls within current quarter if event date is set
        if (dateOfEvent > startOfQuarter && dateOfEvent < endOfQuarter && dateOfEvent != null) {
            // Update chapter score to 0 if null
            if (chapterScore == null) {
                chapterScore = 0;
            }

            // Assign event score based on event type
            switch on eventType {
                when 'Advocacy/Lobbying'{
                    eventScore = 3;
                }
                when 'Workshop' {
                    eventScore = 2;
                }
                when 'Pro-social Event' {
                    eventScore = 2;
                }
                when else {
                    eventScore = 1;
                }
            }

            // If event falls within current quarter, update Chapter's event score
            // System.debug(chapterScore);
            // System.debug(eventScore);
            chapterScore     = chapterScore + eventScore;
            chapter.Score__c = chapterScore;
            update chapter;
            event.Score__c   = eventScore;
        }
    }
}