@isTest

public with sharing class UpdateChapterScoreOnCallTest {
    public static List<Account> createChapters(Integer count) {
        List<Account> chapters = new List<Account>();
        for (Integer i = 0; i < count; i++) {
            Account chapter = new Account();
            // Assign default test values
            chapter.Name                    = 'YPR - Test Chapter, ' + i;
            chapter.RecordTypeId            = '012360000019QQyAAM';
            chapter.Score__c                = 0;
            chapter.Quarterly_Call_Score__c = 0;
            chapters.add(chapter);
        }
        insert chapters;
        return chapters;
    }

    public static List<Account> createNullScoreChapters(Integer count) {
        List<Account> chapters = new List<Account>();
        for (Integer i = 0; i < count; i++) {
            Account chapter = new Account();
            // Assign default test values
            chapter.Name         = 'YPR - Test Chapter, ' + i;
            chapter.RecordTypeId = '012360000019QQyAAM';
            chapters.add(chapter);
        }
        insert chapters;
        return chapters;
    }

    public static void createCallLog(String id) {
        Chapter_Call_Check_In__c callLog = new Chapter_Call_Check_In__c();
        callLog.Account__c = id;
        insert callLog;
    }

    @isTest static void UpdateChapterScoreOnCallTest() {
        List<Account> chapters     = createChapters(10);
        List<Account> nullChapters = createNullScoreChapters(10);
        for (Account chapter : chapters) {
            createCallLog(chapter.Id);
        }
        for (Account chapter : nullChapters) {
            createCallLog(chapter.Id);
        }
    }
}