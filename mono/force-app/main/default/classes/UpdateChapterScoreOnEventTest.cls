@isTest

public class UpdateChapterScoreOnEventTest {
   public static List<Account> createChapters(Integer count) {
        List<Account> chapters = new List<Account>();
        for (Integer i = 0; i < count; i++) {
            Account chapter = new Account();
            // Assign default test values
            chapter.Name                    = 'YPR - Test Chapter, ' + i;
            chapter.RecordTypeId            = '012360000019QQyAAM';
            chapter.Score__c                = 0;
            chapter.Quarterly_Call_Score__c = 0;
            chapters.add(chapter);
        }
        insert chapters;
        return chapters;
    }

    public static List<Account> createNullScoreChapters(Integer count) {
        List<Account> chapters = new List<Account>();
        for (Integer i = 0; i < count; i++) {
            Account chapter = new Account();
            // Assign default test values
            chapter.Name         = 'YPR - Test Chapter, ' + i;
            chapter.RecordTypeId = '012360000019QQyAAM';
            chapters.add(chapter);
        }
        insert chapters;
        return chapters;
    }

    public static void createProSocialEvent(string id) {
        Chapter_Event__c event = new Chapter_Event__c();
        event.Chapter__c       = id;
        event.Event_Type__c    = 'Pro-social Event';
        event.Event_Date__c    = date.Today();
        insert event;
    }

    public static void createAdvocacyEvent(string id) {
        Chapter_Event__c event = new Chapter_Event__c();
        event.Chapter__c       = id;
        event.Event_Type__c    = 'Advocacy/Lobbying';
        event.Event_Date__c    = date.Today();
        insert event;
    }

    public static void createWorkshop(string id) {
        Chapter_Event__c event = new Chapter_Event__c();
        event.Chapter__c       = id;
        event.Event_Type__c    = 'Workshop';
        event.Event_Date__c    = date.Today();
        insert event;
    }

    public static void createARM(string id) {
        Chapter_Event__c event = new Chapter_Event__c();
        event.Chapter__c       = id;
        event.Event_Type__c    = 'All Recovery Meeting';
        event.Event_Date__c    = date.Today();
        insert event;
    }
    

    @isTest static void CreateChapterAndProSocialEventToday() {
        // Create chapter accounts for testing
        for (Account chapter : createChapters(5)) {
            createARM(chapter.Id);
            createWorkshop(chapter.Id);
            createAdvocacyEvent(chapter.Id);
            createProSocialEvent(chapter.Id);
        }
        for (Account chapter : createNullScoreChapters(5)) {
            createARM(chapter.Id);
            createWorkshop(chapter.Id);
            createAdvocacyEvent(chapter.Id);
            createProSocialEvent(chapter.Id);
        }
    }
}